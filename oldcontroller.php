<?php

namespace App\Controller;

use App\Entity\Form;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\User;

/**
 * Class FormController
 * @package App\Controller
 * @Route("/api/form", name="api_form")
 */
class FormController extends AbstractController
{
    /**
     * @Route(methods="POST")
     */
    public function simulator(Request $request, EntityManagerInterface $manager, int $id = null)
    {
        $form = null;
        $mail = MailerInterface::class;

        $user = $this->getUser();
        if ($id == null) {
            $form = new Form();
        } else {
            $repo = $this->getDoctrine()->getRepository(Form::class);
            $form = $repo->find($id);
            if (!$form) {
                return $this->json(null, Response::HTTP_NOT_FOUND);
            }
        }

        // Return an Error at the moment bcs there is no Symfony Form tied to an entity
        $form = $this->createForm(FormType::class, $form, array(
            'crsf_protection' => false
        ));

        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            // Not working since i don't have an "User" Entity so i don't have a Getter/Setter.
            // I can probably delete this and it'll work fine w/o it
            $form->setUser($user);
            $manager->persist($form);
            $manager->flush();

            return $this->json($form, Response::HTTP_CREATED);
        }
        return $this->json(["message" => "Error" . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }
}
