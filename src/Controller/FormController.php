<?php

namespace App\Controller;

use App\Entity\Form;
use App\Form\Form2Type;
use App\Form\FormType;
use App\Repository\FormRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/form", name="api_form")
 */
class FormController extends AbstractController
{
    /**
     * @Route("/", name="form_index", methods={"GET"})
     */
    public function index(FormRepository $formRepository): Response
    {
        return $this->render('form/index.html.twig', [
            'forms' => $formRepository->findAll(),
        ]);
    }

    /**
     * @Route(methods={"GET","POST"})
     */
    public function addPost(Request $request, EntityManagerInterface $manager, int $id = null)
    {

        $form = null;

        $user = $this->getUser();
        if ($id == null) {
            $post = new Form();
        } else {
            $repo = $this->getDoctrine()->getRepository(Form::class);
            $post = $repo->find($id);
            if (!$post) {
                return $this->json(null, Response::HTTP_NOT_FOUND);
            }
        }


        $form = $this->createForm(FormType::class, $post, array(
            'csrf_protection' => false
        ));

        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {

            // $post->setUser($user);
            $manager->persist($post);
            $manager->flush();

            return $this->json($post, Response::HTTP_CREATED);
        }

        return $this->json(["message" => "Error" . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }






    // public function new(Request $request): Response
    // {
    //     $form = new Form();
    //     $form = $this->createForm(FormType::class, $form);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $entityManager = $this->getDoctrine()->getManager();
    //         $entityManager->persist($form);
    //         $entityManager->flush();
    //     }

    //     $form = $this->createForm(FormType::class, $form, array(
    //         'csrf_protection' => false
    //     ));

    //     return $this->render('form/new.html.twig', [
    //         'form' => $form,
    //         'form' => $form->createView(),
    //     ]);
    // }

    /**
     * @Route("/{id}", name="form_show", methods={"GET"})
     */
    public function show(Form $form): Response
    {
        return $this->render('form/show.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="form_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Form $form): Response
    {
        $form = $this->createForm(FormType::class, $form);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('form_index');
        }

        return $this->render('form/edit.html.twig', [
            'form' => $form,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="form_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Form $form): Response
    {
        if ($this->isCsrfTokenValid('delete' . $form->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($form);
            $entityManager->flush();
        }

        return $this->redirectToRoute('form_index');
    }
}
