<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Monolog\Handler\SwiftMailerHandler;
use Swift_Mailer;
use Swift_SmtpTransport;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MailController
 * @package App\Controller
 * @Route("api/email", name="email")
 */
class MailController extends AbstractController
{
    /**
     * @Route(methods="POST")
     */
    public function sendMail(\Swift_Mailer $mailer)
    {

        // $transporter = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
        //     ->setUsername($this->username)
        //     ->setPassword($this->password);

        // $this->mailer = Swift_Mailer::newInstance($transporter);
        // , 'smtp.gmail.com', 465, 'ssl'
        $message = (new \Swift_Message('Test'))
            ->setFrom('thomasaubert69@gmail.com')
            ->setTo('krydemon@gmail.com')
            ->setBody(
                $this->renderView(
                    'mail/index.html.twig'
                ),
                'text/html'
            );
        $mailer->send($message);

        return $this->json(null, Response::HTTP_CREATED);
    }
}
